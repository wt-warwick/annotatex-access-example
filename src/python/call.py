import requests

def query(gql):
    request = requests.post('http://krakow:4000/graphql', json={'query': gql})
    if request.status_code == 200:
        return request.json()
    else:
        raise Exception("Query failed to run by returning code of {}. {}".format(request.status_code, query))


gql = """
{
    annotations {
        sentence
        labels
    }
}
"""

result = query(gql)
print(result)
